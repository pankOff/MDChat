# MDChat
MDChat. Shorter way, better health.
MDChat is app-system based on blockchain-technology. Blockchain makes world better. We want to simplify procedure of getting special medicaments eliminating doctor visit.

What problems does our project solve? 
Online it is impossible to verify the accuracy of the prescription with the buyer, therefore, in pharmacies only OTC drugs.
Anonymous chat between doctor and patient is not exist
Private information about the history of human diseases is not sufficiently stored
The doctor only needs to know the medical history relating to his area.

Solution
The platform for: 
As doctor: give an approvement of buying current medicine for current patient, request the necessary information from the patient’s medical book 
As patient: online purchase prescription medicines,  anonymous chat between doctor and patient

Technical details.
Firstly, patient wants to take the recipe on the medicine. He goes to the doctor. Doctor inputs medical recipe in our blockchain-system. After that, the person who needs to get this medicine simply makes an order at the pharmacy. The pharmacy already knows at the expense of the blockchain system which drugs can be taken and which are not.
Anonymous chat between the doctor and the patient allows the patient to be confident in the preservation of private information when communicating with the doctor

Conclusion. 
In our modern world time is a very important resource. We are aware of this and are making a product that not only optimizes time costs, but also allows you to get any necessary medicine as quickly and efficiently as possible. A medical book on the blockchain will allow you to store the entire history of the patient’s diseases and provide the necessary information to the doctor upon request.
For complete frankness between doctor and patient online, we create secure anonymous chat using platform technology.